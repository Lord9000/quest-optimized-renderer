using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class TestEmissive : MonoBehaviour
{
    public Material m;

    [ContextMenu("Enable baked emissive")]
    public void EnableEmissive()
    {
        m.globalIlluminationFlags = MaterialGlobalIlluminationFlags.BakedEmissive;
    }
}
