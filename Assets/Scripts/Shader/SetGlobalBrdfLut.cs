using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGlobalBrdfLut : MonoBehaviour
{
    public Texture2D brdfLutTexture;

    private void Awake()
    {
        SetBrdfLut();
    }

    private void OnValidate()
    {
        SetBrdfLut();
    }

    private void SetBrdfLut()
    {
        Shader.SetGlobalTexture("_BrdfLUT", brdfLutTexture);
    }
}
