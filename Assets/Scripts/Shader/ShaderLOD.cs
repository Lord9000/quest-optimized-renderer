using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(MeshRenderer))]
public class ShaderLOD : MonoBehaviour
{
    private ShaderLODGlobalSettings globalSettings;
    private Camera mainCamera;
    private MeshRenderer meshRenderer;
    private float distance;
    private int currentLod;

    private void Awake()
    {
        globalSettings = Resources.Load("ShaderLOD_settings") as ShaderLODGlobalSettings;

        meshRenderer = GetComponent<MeshRenderer>();
        mainCamera = Camera.main;
    }

    private void Update()
    {
        distance = Vector3.Distance(mainCamera.transform.position, transform.position);
        if (distance > globalSettings.lod2Distance)
        {
            // LOD 2
            if (currentLod != 2)
            {
                currentLod = 2;
                meshRenderer.material.DisableKeyword("SHADER_TIER0");
                meshRenderer.material.DisableKeyword("SHADER_TIER1");
                meshRenderer.material.EnableKeyword("SHADER_TIER2");
            }
        }
        else if (distance > globalSettings.lod1Distance)
        {
            // LOD 1
            if (currentLod != 1)
            {
                currentLod = 1;
                meshRenderer.material.DisableKeyword("SHADER_TIER0");
                meshRenderer.material.DisableKeyword("SHADER_TIER2");
                meshRenderer.material.EnableKeyword("SHADER_TIER1");
            }
        }
        else
        {
            // LOD 0
            if (currentLod != 0)
            {
                currentLod = 0;
                meshRenderer.material.DisableKeyword("SHADER_TIER2");
                meshRenderer.material.DisableKeyword("SHADER_TIER1");
                meshRenderer.material.EnableKeyword("SHADER_TIER0");
            }
        }
    }
}
