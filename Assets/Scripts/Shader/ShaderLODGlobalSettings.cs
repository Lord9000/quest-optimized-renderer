using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShaderLOD_settings", menuName = "Shader LOD/Settings config")]
public class ShaderLODGlobalSettings : ScriptableObject
{
    public float lod1Distance;
    public float lod2Distance;
    public float cullDistance;
    public bool shouldCull;
}
