using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.XR;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class BenchmarkMesh : MonoBehaviour
{
    public Vector2 dimensions = new Vector2(1.0f, 1.0f);
    public int xSize, ySize;
    public int yAddOnButton = 100;
    private float xInc, yInc;

    private Vector3[] vertices;
    private Mesh mesh;
    private bool lastFrameTriggerValue = false;

    private void Awake()
    {
        Generate();
    }

    private void Update()
    {
        var inputDevices = new List<InputDevice>();
        InputDevices.GetDevices(inputDevices);

        bool triggerValue = false;
        bool gripValue = false;
        foreach (var device in inputDevices)
        {
            device.TryGetFeatureValue(CommonUsages.triggerButton, out triggerValue);
            device.TryGetFeatureValue(CommonUsages.gripButton, out gripValue);
        }

        if ((!lastFrameTriggerValue && triggerValue) || Input.GetKeyDown(KeyCode.Space))
        {
            ySize += yAddOnButton;
            Generate();
        }

        lastFrameTriggerValue = triggerValue;
    }

    private void Generate()
    {
        xInc = dimensions.x / (float)xSize;
        yInc = dimensions.y / (float)ySize;

        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.name = "Procedural Grid";

        vertices = new Vector3[(xSize + 1) * (ySize + 1)];
        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                vertices[i] = new Vector3(x * xInc, y * yInc);
            }
        }

        mesh.vertices = vertices;
        Debug.Log($"Vertex count: {mesh.vertexCount}");

        int[] triangles = new int[xSize * ySize * 6];
        int vi = 0;
        for (int ti = 0, y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize + 2;
            }
        }
        Debug.Log($"Triangle count: {vi * 2}");
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    //private void OnDrawGizmos()
    //{
    //    if (vertices == null)
    //    {
    //        return;
    //    }

    //    Gizmos.color = Color.red;
    //    for (int i = 0; i < vertices.Length; i++)
    //    {
    //        Gizmos.DrawSphere(vertices[i], 0.1f);
    //    }
    //}
}