using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR;
using Random = UnityEngine.Random;

public class TestQuadSpawn : MonoBehaviour
{
    [Header("General setup")]
    public GameObject testQuadPrefab;
    public Vector3 startPosition;
    public Vector3 positionIncrement;
    public bool makeMaterialInstances = false;
    public bool generateTextures = false;
    public bool changeShader = false;
    public int textureDimensions = 16;
    [FormerlySerializedAs("xCount")] public int xMax;
    [FormerlySerializedAs("yCount")] public int yMax;
    [Header("Setup on Awake")]
    public bool spawnOnAwake;
    public int spawnCountOnAwake;
    [Header("Setup on button")] 
    public int spawnCountOnButton;
    [Header("Setup on time")] 
    public bool autostart;
    public int spawnCountOnTime;
    public float timeDelay;
    [Header("Optional shader setup")] 
    public Shader[] shaders;

    private bool isSpawning = false;
    private int maxCount;
    private int currentCount = 0;
    private bool lastFrameTriggerValue = false;
    private bool lastFrameGripValue = false;

    private void Awake()
    {
        maxCount = xMax * yMax;

        if (spawnOnAwake)
        {
            Spawn(spawnCountOnAwake);
        }

        if (autostart)
        {
            StartCoroutine(TimeSpawn());
        }
    }

    private IEnumerator TimeSpawn()
    {
        while (currentCount < maxCount)
        {
            yield return new WaitForSeconds(timeDelay);
            Spawn(spawnCountOnTime);
        }
    }

    private void Update()
    {
        var inputDevices = new List<InputDevice>();
        InputDevices.GetDevices(inputDevices);

        bool triggerValue = false;
        bool gripValue = false;
        foreach (var device in inputDevices)
        {
            device.TryGetFeatureValue(CommonUsages.triggerButton, out triggerValue);
            device.TryGetFeatureValue(CommonUsages.gripButton, out gripValue);
        }

        if ((!lastFrameTriggerValue && triggerValue) || Input.GetKeyDown(KeyCode.Space))
        {
            Spawn(spawnCountOnButton);
        }

        if (!lastFrameGripValue && gripValue)
        {
            if (!isSpawning)
            {
                isSpawning = true;
                StartCoroutine(TimeSpawn());
            }
        }

        lastFrameTriggerValue = triggerValue;
        lastFrameGripValue = gripValue;
    }

    private void Spawn(int count)
    {
        count = Mathf.Clamp(count, 0, maxCount - currentCount);
        for (int i = 0; i < count; i++)
        {
            // ReSharper disable once PossibleLossOfFraction
            var incrementedPosition = new Vector3(positionIncrement.x * (currentCount % xMax), positionIncrement.y * (currentCount / xMax), positionIncrement.z * currentCount);
            var instantiated = Instantiate(testQuadPrefab, startPosition + incrementedPosition, Quaternion.identity);
            if (makeMaterialInstances)
            {
                if (generateTextures)
                {
                    Texture2D tempTex = new Texture2D(textureDimensions, textureDimensions);

                    int dimensionsSquared = textureDimensions * textureDimensions;
                    Color[] colorsToSet = new Color[dimensionsSquared];
                    for (int j = 0; j < dimensionsSquared; j++)
                    {
                        colorsToSet[j] = Random.ColorHSV();
                    }
                    tempTex.SetPixels(colorsToSet);
                    tempTex.Apply();

                    instantiated.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", tempTex);
                }
                else if (changeShader)
                {
                    instantiated.GetComponent<MeshRenderer>().material.shader = shaders[currentCount % shaders.Length];
                }
                else
                {
                    instantiated.GetComponent<MeshRenderer>().material.SetColor("_Color", Random.ColorHSV());
                }
            }
            currentCount++;
        }
    }
}
