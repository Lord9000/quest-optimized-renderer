using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class TestDynamicResolution : MonoBehaviour
{
    public Vector2 minMaxRenderScale = new Vector2(0.1f, 1.5f);
    public int steps = 10;
    public bool invertSteps = false;

    public float RenderScale { get; private set; } = 1.0f;

    private int _currentStep = 0;
    private bool lastFrameTriggerValue = false;
    private List<InputDevice> inputDevices;

    private void OnEnable()
    {
        inputDevices = new List<InputDevice>();
        InputDevices.GetDevices(inputDevices);
        RenderScale = invertSteps ? minMaxRenderScale.y : minMaxRenderScale.x;
        //StartCoroutine(TestViewportScaleChange());
    }

    private void OnDisable()
    {
        RenderScale = 1.0f;
        //StopAllCoroutines();
    }

    private void Update()
    {
        bool triggerValue = false;
        foreach (var device in inputDevices)
        {
            device.TryGetFeatureValue(CommonUsages.triggerButton, out triggerValue);
            if (triggerValue)
                break;
        }

        if ((!lastFrameTriggerValue && triggerValue) || Input.GetKeyDown(KeyCode.Space))
        {
            ViewportScaleStep();
        }

        lastFrameTriggerValue = triggerValue;
    }

    private void ViewportScaleStep()
    {
        if (invertSteps)
            RenderScale = Mathf.Lerp(minMaxRenderScale.y, minMaxRenderScale.x, _currentStep / (float)steps);
        else
            RenderScale = Mathf.Lerp(minMaxRenderScale.x, minMaxRenderScale.y, _currentStep / (float)steps);
        //XRSettings.renderViewportScale = RenderScale;
        XRSettings.eyeTextureResolutionScale = RenderScale;
        Debug.Log($"Set the render scale to: {RenderScale}");
        _currentStep++;
        if (_currentStep > steps)
        {
            _currentStep = 0;
        }
    }

    private IEnumerator TestViewportScaleChange()
    {
        while (true)
        {
            yield return new WaitForSeconds(3.0f);
            ViewportScaleStep();
        }
    }
}
