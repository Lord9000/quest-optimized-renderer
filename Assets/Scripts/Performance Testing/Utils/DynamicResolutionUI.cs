using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DynamicResolutionUI : MonoBehaviour
{
    public TestDynamicResolution testDynamicResolution;
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        text.text = "Resolution Scale: " + testDynamicResolution.RenderScale;
    }
}
