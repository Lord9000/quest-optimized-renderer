using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

[ExecuteAlways]
public class PackRoughnessToMetallic : MonoBehaviour
{
    public string pathInAssets;
    public Texture2D roughnessMap;
    public Texture2D metallicMap;

    [ContextMenu("Pack into metallic")]
    public void Pack()
    {
        if (!roughnessMap)
        {
            Debug.LogError("Roughness texture is required!");
            return;
        }

        int nPixels = roughnessMap.width * roughnessMap.height;
        Color[] packedMap = new Color[nPixels];

        if (!metallicMap)
        {
            for (int i = 0; i < packedMap.Length; i++)
            {
                packedMap[i] = Color.black;
            }
        }
        else if (roughnessMap.width != metallicMap.width || roughnessMap.height != metallicMap.height)
        {
            Debug.LogError($"Textures {roughnessMap} and {metallicMap} have different dimensions. Aborting.");
            return;
        }
        else
        {
            // Getting metallic values
            for (int y = 0, i = 0; y < metallicMap.height; y++)
            {
                for (int x = 0; x < metallicMap.width; x++, i++)
                {
                    packedMap[i] = metallicMap.GetPixel(x, y);
                }
            }
        }

        // Packing inverted roughness values
        for (int y = 0, i = 0; y < roughnessMap.height; y++)
        {
            for (int x = 0; x < roughnessMap.width; x++, i++)
            {
                packedMap[i].a = 1.0f - roughnessMap.GetPixel(x, y).r;
            }
        }

        Texture2D packedTexture = new Texture2D(roughnessMap.width, roughnessMap.height);
        packedTexture.SetPixels(packedMap);

        byte[] encodedTex = packedTexture.EncodeToPNG();
        string path = Application.dataPath + "/" + pathInAssets + roughnessMap.name.Substring(0, roughnessMap.name.LastIndexOf('_')) + "_metallicPacked.png";
        System.IO.File.WriteAllBytes(path, encodedTex);
        Debug.Log($"<color=green>Succesfully wrote packed texture to {path}</color>");
    }
}
