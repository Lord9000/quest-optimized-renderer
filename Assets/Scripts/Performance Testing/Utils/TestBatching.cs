using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR;
using Random = UnityEngine.Random;

public class TestBatching : MonoBehaviour
{
    [Header("General setup")]
    public GameObject testQuadPrefab;
    public Vector3 startPosition;
    public Vector3 positionIncrement;
    public bool makeMaterialInstances = false;
    public bool makeStatic = false;
    public bool makeInstanced = false;
    public bool combineMeshes = false;
    public Material[] instanceMaterials;
    [FormerlySerializedAs("xCount")] public int xMax;
    [FormerlySerializedAs("yCount")] public int yMax;
    [Header("Setup on Awake")]
    public bool spawnOnAwake;
    public int spawnCountOnAwake;
    [Header("Setup on button")] 
    public int spawnCountOnButton;
    [Header("Setup on time")] 
    public bool autostart;
    public int spawnCountOnTime;
    public float timeDelay;

    private bool isSpawning = false;
    private int maxCount;
    private int currentCount = 0;
    private bool lastFrameTriggerValue = false;
    private bool lastFrameGripValue = false;

    private List<GameObject> spawnedQuads;
    private List<GameObject> combinedQuads;

    private void Awake()
    {
        spawnedQuads = new List<GameObject>();
        combinedQuads = new List<GameObject>();
        maxCount = xMax * yMax;

        foreach (var instanceMaterial in instanceMaterials)
        {
            instanceMaterial.enableInstancing = makeInstanced;
        }

        if (spawnOnAwake)
        {
            Spawn(spawnCountOnAwake);
        }

        if (autostart)
        {
            StartCoroutine(TimeSpawn());
        }
    }

    private IEnumerator TimeSpawn()
    {
        while (currentCount < maxCount)
        {
            yield return new WaitForSeconds(timeDelay);
            Spawn(spawnCountOnTime);
        }
    }

    private void Update()
    {
        var inputDevices = new List<InputDevice>();
        InputDevices.GetDevices(inputDevices);

        bool triggerValue = false;
        bool gripValue = false;
        foreach (var device in inputDevices)
        {
            device.TryGetFeatureValue(CommonUsages.triggerButton, out triggerValue);
            device.TryGetFeatureValue(CommonUsages.gripButton, out gripValue);
        }

        if ((!lastFrameTriggerValue && triggerValue) || Input.GetKeyDown(KeyCode.Space))
        {
            Spawn(spawnCountOnButton);
        }

        if (!lastFrameGripValue && gripValue)
        {
            if (!isSpawning)
            {
                isSpawning = true;
                StartCoroutine(TimeSpawn());
            }
        }

        lastFrameTriggerValue = triggerValue;
        lastFrameGripValue = gripValue;
    }

    [ContextMenu("Spawn 100 quads")]
    private void Spawn100()
    {
        maxCount = xMax * yMax;
        Spawn(100);
    }

    [ContextMenu("Reset counters")]
    private void ResetCounters()
    {
        isSpawning = false;
        maxCount = xMax * yMax;
        currentCount = 0;
    }

    private void Spawn(int count)
    {
        foreach (var spawnedQuad in spawnedQuads)
        {
            Destroy(spawnedQuad);
        }
        spawnedQuads.Clear();

        count = Mathf.Clamp(count + currentCount, 0, maxCount);
        currentCount = 0;

        for (int i = 0; i < count; i++)
        {
            // ReSharper disable once PossibleLossOfFraction
            Vector3 incrementedPosition = new Vector3(positionIncrement.x * (float)(currentCount % xMax), positionIncrement.y * (currentCount / xMax), startPosition.z);
            var instantiated = Instantiate(testQuadPrefab, startPosition + incrementedPosition, Quaternion.identity, transform);
            spawnedQuads.Add(instantiated);

            if (makeMaterialInstances)
            {
                instantiated.GetComponent<MeshRenderer>().sharedMaterial = instanceMaterials[currentCount % instanceMaterials.Length];
            }

            if (makeStatic)
            {
                instantiated.isStatic = true;
            }
            currentCount++;
        }

        if (makeStatic)
        {
            StaticBatchingUtility.Combine(gameObject);
        }

        if (combineMeshes)
        {
            CombineMeshInstances();
        }
    }

    private void CombineMeshInstances()
    {
        foreach (var quad in combinedQuads)
        {
            Destroy(quad);
        }
        combinedQuads.Clear();

        Dictionary<Material, List<MeshFilter>> meshFilters = new Dictionary<Material, List<MeshFilter>>();
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (var renderer in renderers)
        {
            if (!meshFilters.ContainsKey(renderer.sharedMaterial))
            {
                meshFilters.Add(renderer.sharedMaterial, new List<MeshFilter>());
            }
            meshFilters[renderer.sharedMaterial].Add(renderer.GetComponent<MeshFilter>());
        }

        int count = 0;
        foreach (KeyValuePair<Material, List<MeshFilter>> keyValuePair in meshFilters)
        {
            CombineInstance[] combine = new CombineInstance[keyValuePair.Value.Count];
            int i = 0;
            while (i < keyValuePair.Value.Count)
            {
                combine[i].mesh = keyValuePair.Value[i].sharedMesh;
                combine[i].transform = keyValuePair.Value[i].transform.localToWorldMatrix;
                //meshFilters[i].gameObject.SetActive(false);
                Destroy(keyValuePair.Value[i].gameObject);

                i++;
            }

            var combined = new GameObject("combined_" + count);
            combinedQuads.Add(combined);
            var mf = combined.AddComponent<MeshFilter>();
            mf.mesh = new Mesh();
            mf.mesh.CombineMeshes(combine);
            var mr = combined.AddComponent<MeshRenderer>();
            mr.sharedMaterial = keyValuePair.Key;
            count++;
        }
    }
}
