using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDynamicFFR : MonoBehaviour
{
    public int FfrLevel = 3;

    private void Awake()
    {
        Unity.XR.Oculus.Utils.SetFoveationLevel(FfrLevel);
    }
}
