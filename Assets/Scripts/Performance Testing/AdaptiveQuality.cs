using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

[RequireComponent(typeof(Camera))]
public class AdaptiveQuality : MonoBehaviour
{
    enum QualityState
    {
        Minimum,
        Balanced,
        Maximum
    }

    public bool enableAdaptiveQuality = true;

    [Header("Render scale setup")]
    public bool includeFfrAdjustments = false;
    [Range(1.0f, 4.0f)]public int maxFfrLevel = 3;
    [Tooltip("Minimum and maximum render scale. If max scale exceeds 1.0, the system will automatically perform oversampling if enough performance headroom is available. " +
             "Going below 0.75 minimum render scale may severly decrease visual quality and should be used for testing only.")]
    public Vector2 minMaxRenderScale = new Vector2(0.8f, 1.0f);
    [Tooltip("How big one quality step should be. Lower values give less noticeable transitions, but increase system inertia and may have negative impact on overall performance.")]
    public float renderScaleStep = 0.025f;

    [Header("Adaptive parameters")]
    [Tooltip("How frequently the counters are sampled. Lower values give faster response but less filtering and possibly more performance overhead")]
    public float sampleInterval = 0.1f;
    [Tooltip("How many seconds the system should wait before another adjustment can be made. This will prevent abrupt quality changes but induce latency.")]
    public float adjustmentCooldownTime = 1.0f;

    [Header("App metrics setup")]
    // GPU TIME PARAMETER ==================================================
    [SerializeField] private bool useGpuTimeThreshold = false;
    [Tooltip("Gpu time in ms. Target for 72 FPS is 13.8 ms, however you should target lower values to leave time for compositor and cpu")]
    public float highGpuTimeThreshold = 12.0f;
    public float lowGpuTimeThreshold = 9.0f;
    [Tooltip("How many frames should be averaged to adjust the resolution")]
    public int gpuTimeSampleCount = 4;
    // GPU UTILIZATION PARAMETER ===========================================
    [SerializeField] private bool useGpuUtilizationThreshold = false;
    [Range(0.0f, 1.0f)] public float highGpuUtilizationThreshold = 0.9f;
    [Range(0.0f, 1.0f)] public float lowGpuUtilizationThreshold = 0.7f;
    public int gpuUtilizationSampleCount = 4;
    // CPU UTILIZATION PARAMETER ===========================================
    [SerializeField] private bool useCpuUtilizationThreshold = false;
    [Range(0.0f, 1.0f)] public float highCpuUtilizationThreshold = 0.9f;
    [Range(0.0f, 1.0f)] public float lowCpuUtilizationThreshold = 0.7f;
    public int cpuUtilizationSampleCount = 4;
    // BATTERY TEMP PARAMETER ==============================================
    // TODO: Check if getting the battery temp is possible
    //[SerializeField] private bool useBatteryTempThreshold = false;
    //[Tooltip("Battery temperature in degrees Celsius.")]
    //public float highBatteryTempThreshold = 35.0f;
    //public float lowBatteryTempThreshold = 32.0f;

    private QualityState currentQualityState;

    private new Camera camera;
    private float sampleAccumulatedTime = 0.0f;
    private float cooldownAccumulatedTime = 0.0f;
    private bool isInCooldownState = false;

    private float[] gpuTimeSamples;
    private int gpuTimeSamplesIndex = 0;

    private float[] gpuUtilizationSamples;
    private int gpuUtilizationSamplesIndex = 0;

    private float[] cpuUtilizationSamples;
    private int cpuUtilizationSamplesIndex = 0;

    private void Awake()
    {
        camera = GetComponent<Camera>();
        if (!camera.allowDynamicResolution)
        {
            Debug.LogWarning($"Allow dynamic resolution was disabled on attached camera ({camera.name}). Overriding to true, otherwise dynamic resolution will not work properly.");
            camera.allowDynamicResolution = true;
        }

        // If the max render scale is > 1.0, then set base scale to 1.0 and increase from there
        XRSettings.eyeTextureResolutionScale = Mathf.Min(1.0f, minMaxRenderScale.y);
        // Set current quality state according to set eye texture resolution
        currentQualityState = XRSettings.eyeTextureResolutionScale < minMaxRenderScale.y ? QualityState.Balanced : QualityState.Maximum;

        // Enable performance metrics
        Unity.XR.Oculus.Stats.PerfMetrics.EnablePerfMetrics(true);

        // Create sample buffers array only if specific performance samplers are enabled
        if (useGpuTimeThreshold)
            gpuTimeSamples = new float[gpuTimeSampleCount];

        if (useGpuUtilizationThreshold)
            gpuUtilizationSamples = new float[gpuUtilizationSampleCount];

        if (useCpuUtilizationThreshold)
            cpuUtilizationSamples = new float[cpuUtilizationSampleCount];
    }

    private void OnPreCull()
    {
        // Handle sample interval
        sampleAccumulatedTime += Time.deltaTime;

        // Handle adjustment cooldown
        if (isInCooldownState)
        {
            cooldownAccumulatedTime += Time.deltaTime;
            
            if (cooldownAccumulatedTime >= adjustmentCooldownTime)
            {
                cooldownAccumulatedTime = 0.0f;
                isInCooldownState = false;
            }
        }

        // Performance sampling
        if (sampleAccumulatedTime >= sampleInterval)
        {
            sampleAccumulatedTime = 0.0f;

            if (useGpuTimeThreshold)
                SampleGpuTime();

            if (useGpuUtilizationThreshold)
                SampleGpuUtilization();

            if (useCpuUtilizationThreshold)
                SampleCpuUtilization();
        }
        
        // Adaptive quality adjustments
        if (enableAdaptiveQuality)
        {
            if (useGpuTimeThreshold && !isInCooldownState)
            {
                isInCooldownState = AdjustQuality(GetAverage(gpuTimeSamples), lowGpuTimeThreshold, highGpuTimeThreshold);
            }

            if (useGpuUtilizationThreshold && !isInCooldownState)
            {
                isInCooldownState = AdjustQuality(GetAverage(gpuUtilizationSamples), lowGpuUtilizationThreshold, highGpuUtilizationThreshold);
            }

            if (useCpuUtilizationThreshold && !isInCooldownState)
            {
                isInCooldownState = AdjustQuality(GetAverage(cpuUtilizationSamples), lowCpuUtilizationThreshold, highCpuUtilizationThreshold);
            }
        }
        else
        {
            XRSettings.eyeTextureResolutionScale = Mathf.Min(1.0f, minMaxRenderScale.y);
        }
        
    }

    private bool AdjustQuality(float sample, float min, float max)
    {
        bool wasAdjusted = false;
        if (currentQualityState != QualityState.Minimum && sample > max)
        {
            if (includeFfrAdjustments && Unity.XR.Oculus.Utils.GetFoveationLevel() < maxFfrLevel)
            {
                // Increase FFR level
                Unity.XR.Oculus.Utils.SetFoveationLevel(Unity.XR.Oculus.Utils.GetFoveationLevel() + 1);
            }
            else
            {
                // Decrease eye resolution quality
                XRSettings.eyeTextureResolutionScale -= renderScaleStep;
                currentQualityState = XRSettings.eyeTextureResolutionScale <= minMaxRenderScale.x ? QualityState.Minimum : QualityState.Balanced;
            }
            wasAdjusted = true;
        }
        else if (currentQualityState != QualityState.Maximum && sample < min)
        {
            if (includeFfrAdjustments && Unity.XR.Oculus.Utils.GetFoveationLevel() > 0 && XRSettings.eyeTextureResolutionScale >= 1.0f)
            {
                // Increase FFR level
                Unity.XR.Oculus.Utils.SetFoveationLevel(Unity.XR.Oculus.Utils.GetFoveationLevel() - 1);
            }
            else
            {
                // Increase quality
                XRSettings.eyeTextureResolutionScale += renderScaleStep;
                currentQualityState = XRSettings.eyeTextureResolutionScale >= minMaxRenderScale.y ? QualityState.Maximum : QualityState.Balanced;
            }
            wasAdjusted = true;
        }

        return wasAdjusted;
    }

    private void SampleGpuTime()
    {
        gpuTimeSamples[gpuTimeSamplesIndex] = Unity.XR.Oculus.Stats.PerfMetrics.AppGPUTime * 1000.0f;
        gpuTimeSamplesIndex = (gpuTimeSamplesIndex + 1) % gpuTimeSampleCount;
    }

    private void SampleGpuUtilization()
    {
        gpuUtilizationSamples[gpuUtilizationSamplesIndex] = Unity.XR.Oculus.Stats.PerfMetrics.GPUUtilization;
        gpuUtilizationSamplesIndex = (gpuUtilizationSamplesIndex + 1) % gpuUtilizationSampleCount;
    }

    private void SampleCpuUtilization()
    {
        // TODO: Mode CPUUtilizationWorst may also be a viable option 
        cpuUtilizationSamples[cpuUtilizationSamplesIndex] = Unity.XR.Oculus.Stats.PerfMetrics.CPUUtilizationAverage;
        cpuUtilizationSamplesIndex = (cpuUtilizationSamplesIndex + 1) % cpuUtilizationSampleCount;
    }

    private float GetAverage(float[] array)
    {
        float average = 0.0f;
        foreach (var sample in array)
        {
            average += sample;
        }

        average /= array.Length;
        
        return average;
    }
}
