using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class AdaptiveQualityVisualizer : MonoBehaviour
{
    public Color minColor = Color.red;
    public Color midColor = Color.green;
    public Color maxColor = Color.blue;
    public AdaptiveQuality adaptiveQualitySystem;
    public Slider renderScaleSlider;
    public Image sliderBackground;
    public Image sliderFill;
    public Text minScaleText;
    public Text maxScaleText;
    public Text valueText;
    public Text gpuTimeText;
    public Text gpuUsageText;
    public Text cpuTimeText;
    public Text cpuUsageText;

    private void Awake()
    {
        minScaleText.text = adaptiveQualitySystem.minMaxRenderScale.x.ToString(CultureInfo.InvariantCulture);
        maxScaleText.text = adaptiveQualitySystem.minMaxRenderScale.y.ToString(CultureInfo.InvariantCulture);

        if (gpuUsageText || cpuUsageText)
            Unity.XR.Oculus.Stats.PerfMetrics.EnablePerfMetrics(true);

        renderScaleSlider.minValue = adaptiveQualitySystem.minMaxRenderScale.x;
        renderScaleSlider.maxValue = adaptiveQualitySystem.minMaxRenderScale.y;
    }

    void Update()
    {
        float currentResolutionScale = XRSettings.eyeTextureResolutionScale;
        renderScaleSlider.value = currentResolutionScale;
        valueText.text = currentResolutionScale.ToString(CultureInfo.InvariantCulture);
        Color currentColor = Color.white;
        if (currentResolutionScale <= 1.0f)
            currentColor = Color.Lerp(minColor, midColor, currentResolutionScale);
        else
            currentColor = Color.Lerp(midColor, maxColor, currentResolutionScale - 1.0f);
        Color backgroundColor = new Color(currentColor.r, currentColor.g, currentColor.b, sliderBackground.color.a);
        sliderBackground.color = backgroundColor;
        sliderFill.color = currentColor;

        if (gpuTimeText)
            gpuTimeText.text = "GPU time (ms): " + (Unity.XR.Oculus.Stats.PerfMetrics.AppGPUTime * 1000.0f).ToString(CultureInfo.InvariantCulture);

        if (gpuUsageText)
            gpuUsageText.text = "GPU usage (%): " + Unity.XR.Oculus.Stats.PerfMetrics.GPUUtilization.ToString(CultureInfo.InvariantCulture);

        if (cpuTimeText)
            cpuTimeText.text = "CPU time (ms): " + (Unity.XR.Oculus.Stats.PerfMetrics.AppCPUTime * 1000.0f).ToString(CultureInfo.InvariantCulture);

        if (cpuUsageText)
            cpuUsageText.text = "CPU usage (%): " + Unity.XR.Oculus.Stats.PerfMetrics.CPUUtilizationAverage.ToString(CultureInfo.InvariantCulture);
    }
}
