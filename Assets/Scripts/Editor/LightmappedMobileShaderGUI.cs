using System.Runtime.InteropServices;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;

public class LightmappedMobileShaderGUI : ShaderGUI
{
    public enum ShaderTier
    {
        Tier0,
        Tier1,
        Tier2
    }

    enum RenderingMode
    {
        Opaque,
        Cutout,
        AlphaToCoverage,
        Fade,
        Transparent
    }

    struct RenderingSettings
    {
        public RenderQueue queue;
        public string renderType;
        public BlendMode srcBlend, dstBlend;
        public bool zWrite;
        public bool alphaToMask;

        public static RenderingSettings[] modes =
        {
            new RenderingSettings()
            {
                queue = RenderQueue.Geometry,
                renderType = "",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.Zero,
                zWrite = true
            },
            new RenderingSettings()
            {
                queue = RenderQueue.AlphaTest,
                renderType = "TransparentCutout",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.Zero,
                zWrite = true,
                alphaToMask = false
            },
            new RenderingSettings()
            {
                queue = RenderQueue.AlphaTest,
                renderType = "TransparentCutout",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.Zero,
                zWrite = true,
                alphaToMask = true
            },
            new RenderingSettings()
            {
                queue = RenderQueue.Transparent,
                renderType = "Transparent",
                srcBlend = BlendMode.SrcAlpha,
                dstBlend = BlendMode.OneMinusSrcAlpha,
                zWrite = false,
                alphaToMask = false
            },
            new RenderingSettings()
            {
                queue = RenderQueue.Transparent,
                renderType = "Transparent",
                srcBlend = BlendMode.One,
                dstBlend = BlendMode.OneMinusSrcAlpha,
                zWrite = false,
                alphaToMask = false
            }
        };
    }

    static GUIContent staticLabel = new GUIContent();

    Material target;
    MaterialEditor editor;
    MaterialProperty[] properties;
    bool shouldShowAlphaCutoff;
    private bool showPackingConvention;
    private bool enableMoreParameters;
    private bool overrideShaderTier;

    public override void OnGUI(MaterialEditor editor, MaterialProperty[] properties)
    {
        this.target = editor.target as Material;
        this.editor = editor;
        this.properties = properties;

        DoRenderingMode();
        DoMain();
    }

    void DoMain()
    {
        EditorGUI.BeginChangeCheck();
        enableMoreParameters = DoKeywordCheckbox("More parameters", "_MORE_PARAMETERS");
        if (enableMoreParameters)
        {
            EditorGUILayout.HelpBox("Flexible parameters include more shader instructions, which may be baked into textures for performance. Use only for prototyping or when sure you have enough GPU juice.", MessageType.Warning);
        }
        else if (EditorGUI.EndChangeCheck())
        {
            SetKeyword("_USE_BUMP_SCALE", false);
            SetKeyword("_USE_METALLIC_MULTIPLIER", false);
            SetKeyword("_USE_SMOOTHNESS_MULTIPLIER", false);
            SetKeyword("_USE_OCCLUSION_MULTIPLIER", false);
        }

        EditorGUI.BeginChangeCheck();
        overrideShaderTier = DoKeywordCheckbox("Override shader tier", "_OVERRIDE_SHADER_TIER");
        if (overrideShaderTier)
        {
            DoOverrideTier();
        }
        else if(EditorGUI.EndChangeCheck())
        {
            SetKeyword("SHADER_TIER0", true);
            SetKeyword("SHADER_TIER1", false);
            SetKeyword("SHADER_TIER2", false);
        }

        showPackingConvention = EditorGUILayout.BeginFoldoutHeaderGroup(showPackingConvention, "Texture array packing convention");
        if (showPackingConvention)
        {
            GUILayout.Label("Packing convention:\n0 - Albedo\n1 - Metallic/Smoothness(A)\n2 - Normal\n3 - Occlusion\n4 - Emissive", EditorStyles.label);
        }

        MaterialProperty mainTex = FindProperty("_MainTexArray");
        editor.TexturePropertySingleLine(
            MakeLabel(mainTex, "Albedo (RGB)"), mainTex, FindProperty("_Color")
        );
        if (shouldShowAlphaCutoff)
        {
            DoSlider("_Cutoff");
        }

        bool normalEnabled = DoKeywordCheckbox("Use Normal Map", "_NORMAL_MAP");
        if (enableMoreParameters && normalEnabled)
        {
            EditorGUI.indentLevel++;
            GUI.backgroundColor = Color.red;
            GUI.color = Color.red;
            if (DoKeywordCheckbox("Normal Scale", "_USE_BUMP_SCALE"))
                editor.FloatProperty(FindProperty("_BumpScale"), "Normal scale");
            GUI.color = Color.white;
            GUI.backgroundColor = Color.white;
            EditorGUI.indentLevel--;
        }
        DoKeywordCheckbox("Use Metallic Map", "_METALLIC_MAP");
        if (enableMoreParameters)
        {
            EditorGUI.indentLevel++;
            GUI.backgroundColor = Color.red;
            GUI.color = Color.red;
            if (DoKeywordCheckbox("Metallic Scale", "_USE_METALLIC_MULTIPLIER"))
                DoSlider("_Metallic");
            if (DoKeywordCheckbox("Smoothness Scale", "_USE_SMOOTHNESS_MULTIPLIER"))
                DoSlider("_Smoothness");
            GUI.color = Color.white;
            GUI.backgroundColor = Color.white;
            EditorGUI.indentLevel--;
        }
        bool occlusionEnabled = DoKeywordCheckbox("Use Occlusion Map", "_OCCLUSION_MAP");
        if (enableMoreParameters && occlusionEnabled)
        {
            EditorGUI.indentLevel++;
            GUI.backgroundColor = Color.red;
            GUI.color = Color.red;
            if (DoKeywordCheckbox("Occlusion Multiplier", "_USE_OCCLUSION_MULTIPLIER"))
                DoSlider("_Occlusion");
            GUI.color = Color.white;
            GUI.backgroundColor = Color.white;
            EditorGUI.indentLevel--;
        }

        if (DoKeywordCheckbox("Use Emission Map", "_EMISSION_MAP"))
        {
            EditorGUI.indentLevel++;
            if (DoKeywordCheckbox("Use Emission Color", "_USE_EMISSION_COLOR"))
                DoEmission();
            EditorGUI.indentLevel--;
        }

        GUILayout.Label("Additional config", EditorStyles.boldLabel);
        DoKeywordCheckbox("Use box projection", "_USE_BOX_PROJECTION");
        DoKeywordCheckbox("Force accurate reflections", "_USE_PER_PIXEL_VIEW_DIR");
    }

    bool DoKeywordCheckbox(string label, string keyword)
    {
        bool isKeywordEnabled = EditorGUILayout.Toggle(label, IsKeywordEnabled(keyword));
        if (isKeywordEnabled)
        {
            SetKeyword(keyword, true);
        }
        else
        {
            SetKeyword(keyword, false);
        }

        return isKeywordEnabled;
    }

    void DoSlider(string propName)
    {
        MaterialProperty slider = FindProperty(propName);
        EditorGUI.indentLevel += 2;
        editor.ShaderProperty(slider, MakeLabel(slider));
        EditorGUI.indentLevel -= 2;
    }

    void DoEmission()
    {
        EditorGUI.BeginChangeCheck();
        var prop = FindProperty("_Emission");
        prop.colorValue = EditorGUILayout.ColorField(
            MakeLabel("Emission Color", "HDR Color to be multiplied with emission map"), prop.colorValue, true, false,
            true);
        if (EditorGUI.EndChangeCheck())
        {
            foreach (Material m in editor.targets)
            {
                m.globalIlluminationFlags =
                    MaterialGlobalIlluminationFlags.BakedEmissive;
            }
        }
    }

    void DoRenderingMode()
    {
        RenderingMode mode = RenderingMode.Opaque;
        shouldShowAlphaCutoff = false;
        if (IsKeywordEnabled("_RENDERING_CUTOUT"))
        {
            mode = RenderingMode.Cutout;
            shouldShowAlphaCutoff = true;
        }
        else if (IsKeywordEnabled("_RENDERING_ALPHA_TO_COVERAGE"))
        {
            mode = RenderingMode.AlphaToCoverage;
        }
        else if (IsKeywordEnabled("_RENDERING_FADE"))
        {
            mode = RenderingMode.Fade;
        }
        else if (IsKeywordEnabled("_RENDERING_TRANSPARENT"))
        {
            mode = RenderingMode.Transparent;
        }

        EditorGUI.BeginChangeCheck();
        mode = (RenderingMode) EditorGUILayout.EnumPopup(
            MakeLabel("Rendering Mode"), mode
        );
        if (EditorGUI.EndChangeCheck())
        {
            RecordAction("Rendering Mode");
            SetKeyword("_RENDERING_CUTOUT", mode == RenderingMode.Cutout);
            SetKeyword("_RENDERING_ALPHA_TO_COVERAGE", mode == RenderingMode.AlphaToCoverage);
            SetKeyword("_RENDERING_FADE", mode == RenderingMode.Fade);
            SetKeyword("_RENDERING_TRANSPARENT", mode == RenderingMode.Transparent);

            RenderingSettings settings = RenderingSettings.modes[(int) mode];
            foreach (Material m in editor.targets)
            {
                m.renderQueue = (int) settings.queue;
                m.SetOverrideTag("RenderType", settings.renderType);
                m.SetInt("_SrcBlend", (int) settings.srcBlend);
                m.SetInt("_DstBlend", (int) settings.dstBlend);
                m.SetInt("_ZWrite", settings.zWrite ? 1 : 0);
                m.SetInt("_AlphaToMask", settings.alphaToMask ? 1 : 0);
            }
        }

        //if (mode == RenderingMode.Fade || mode == RenderingMode.Transparent)
        //{
        //    DoSemitransparentShadows();
        //}
    }

    void DoOverrideTier()
    {
        ShaderTier tier = ShaderTier.Tier0;
        if (IsKeywordEnabled("SHADER_TIER1"))
        {
            tier = ShaderTier.Tier1;
        }
        else if (IsKeywordEnabled("SHADER_TIER2"))
        {
            tier = ShaderTier.Tier2;
        }

        EditorGUI.BeginChangeCheck();
        tier = (ShaderTier)EditorGUILayout.EnumPopup(
            MakeLabel("Override Tier"), tier
        );
        if (EditorGUI.EndChangeCheck())
        {
            RecordAction("Override Tier");
            SetKeyword("SHADER_TIER0", tier == ShaderTier.Tier0);
            SetKeyword("SHADER_TIER1", tier == ShaderTier.Tier1);
            SetKeyword("SHADER_TIER2", tier == ShaderTier.Tier2);
        }
    }

    void DoSemitransparentShadows()
    {
        EditorGUI.BeginChangeCheck();
        bool semitransparentShadows =
            EditorGUILayout.Toggle(
                MakeLabel("Semitransp. Shadows", "Semitransparent Shadows"),
                IsKeywordEnabled("_SEMITRANSPARENT_SHADOWS")
            );
        if (EditorGUI.EndChangeCheck())
        {
            SetKeyword("_SEMITRANSPARENT_SHADOWS", semitransparentShadows);
        }

        if (!semitransparentShadows)
        {
            shouldShowAlphaCutoff = true;
        }
    }

    MaterialProperty FindProperty(string name)
    {
        return FindProperty(name, properties);
    }

    static GUIContent MakeLabel(string text, string tooltip = null)
    {
        staticLabel.text = text;
        staticLabel.tooltip = tooltip;
        return staticLabel;
    }

    static GUIContent MakeLabel(MaterialProperty property, string tooltip = null)
    {
        staticLabel.text = property.displayName;
        staticLabel.tooltip = tooltip;
        return staticLabel;
    }

    void SetKeyword(string keyword, bool state)
    {
        if (state)
        {
            foreach (Material m in editor.targets)
            {
                m.EnableKeyword(keyword);
            }
        }
        else
        {
            foreach (Material m in editor.targets)
            {
                m.DisableKeyword(keyword);
            }
        }
    }

    bool IsKeywordEnabled(string keyword)
    {
        return target.IsKeywordEnabled(keyword);
    }

    void RecordAction(string label)
    {
        editor.RegisterPropertyChangeUndo(label);
    }
}