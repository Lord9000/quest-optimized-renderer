using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.Experimental.Rendering;

public class ArrayTexture2DUtility : EditorWindow
{
    public Texture2D[] texturesToCombine;
    public Texture2D albedoTexture2D;
    public Texture2D metallicTexture2D;
    public Texture2D normalTexture2D;
    public Texture2D occlusionTexture2D;
    public Texture2D emissionTexture2D;

    private SerializedObject so;
    private SerializedProperty albedoProperty;
    private SerializedProperty metallicProperty;
    private SerializedProperty normalProperty;
    private SerializedProperty occlusionProperty;
    private SerializedProperty emissionProperty;

    private bool includeEmission = false;
    private bool includeOcclusion = false;
    private string currentPath;

    [MenuItem("Utils/Create 2D Array Texture Asset")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ArrayTexture2DUtility));
    }

    private void OnEnable()
    {
        so = new SerializedObject(this);
        albedoProperty = so.FindProperty("albedoTexture2D");
        metallicProperty = so.FindProperty("metallicTexture2D");
        normalProperty = so.FindProperty("normalTexture2D");
        occlusionProperty = so.FindProperty("occlusionTexture2D");
        emissionProperty = so.FindProperty("emissionTexture2D");
    }

    private void OnSelectionChange()
    {
        if (Selection.activeObject)
        {
            currentPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            Debug.Log($"Current path: {currentPath}");
        }
    }

    void OnGUI()
    {
        EditorGUILayout.LabelField("Basic packing convention:");
        EditorGUILayout.LabelField("0. Albedo");
        EditorGUILayout.LabelField("1. Metallic/Smoothness");
        EditorGUILayout.LabelField("2. Normal");
        EditorGUILayout.LabelField("3. Occlusion");
        EditorGUILayout.LabelField("4. Emissive");
        EditorGUILayout.LabelField("Leave any field empty to fill with basic black texture");
        EditorGUILayout.PropertyField(albedoProperty);
        EditorGUILayout.PropertyField(metallicProperty);
        EditorGUILayout.PropertyField(normalProperty);
        includeOcclusion = EditorGUILayout.Toggle("Include occlusion?", includeOcclusion);
        if (includeOcclusion)
        {
            EditorGUILayout.PropertyField(occlusionProperty);
        }
        includeEmission = EditorGUILayout.Toggle("Include emission?", includeEmission);
        if (includeEmission)
        {
            EditorGUILayout.PropertyField(emissionProperty);
        }
        so.ApplyModifiedProperties();
        if (GUILayout.Button("Pack texture"))
        {
            var path = EditorUtility.SaveFilePanelInProject("Combined asset path", "Texture2DArray", "asset", "Saved", currentPath);
            SaveArray(path);
        }
    }

    private void SaveArray(string path)
    {
        if (includeEmission && includeOcclusion)
            texturesToCombine = new Texture2D[5];
        else if (includeEmission || includeOcclusion)
            texturesToCombine = new Texture2D[4];
        else
            texturesToCombine = new Texture2D[3];

        if (albedoTexture2D != null)
            texturesToCombine[0] = albedoTexture2D;
        else
            texturesToCombine[0] = Texture2D.blackTexture;


        if (metallicTexture2D != null)
            texturesToCombine[1] = metallicTexture2D;
        else
        {
            texturesToCombine[1] = MakeTexture(texturesToCombine[0].width, texturesToCombine[0].height,
                texturesToCombine[0].format, new Color(0.0f, 0.0f, 0.0f, 0.0f));
        }

        if (normalTexture2D != null)
            texturesToCombine[2] = normalTexture2D;
        else
        {
            texturesToCombine[2] = MakeTexture(texturesToCombine[0].width, texturesToCombine[0].height,
                texturesToCombine[0].format, new Color(0.5f, 0.5f, 1.0f, 1.0f));
        }

        if (includeOcclusion)
        {
            if (occlusionTexture2D != null)
                texturesToCombine[3] = occlusionTexture2D;
            else
            {
                texturesToCombine[3] = MakeTexture(texturesToCombine[0].width, texturesToCombine[0].height,
                    texturesToCombine[0].format, new Color(1.0f, 1.0f, 1.0f, 1.0f));
            }
        }

        if (includeEmission)
        {
            if (emissionTexture2D != null)
                texturesToCombine[4] = emissionTexture2D;
            else
            {
                texturesToCombine[4] = MakeTexture(texturesToCombine[0].width, texturesToCombine[0].height,
                    texturesToCombine[0].format, new Color(0.0f, 0.0f, 0.0f, 1.0f));
            }
        }


        // Validation
        int width = texturesToCombine[0].width, height = texturesToCombine[0].height;
        for (int i = 1; i < texturesToCombine.Length; i++)
        {
            if (texturesToCombine[i].width != width || texturesToCombine[i].height != height)
            {
                EditorUtility.DisplayDialog("Invalid texture",
                    "Texture dimensions do not match. Make sure all textures to combine are the same size!\nAborting...", "Dismiss");
                return;
            }
        }

        Texture2DArray texture2DArray = new Texture2DArray(width, height, texturesToCombine.Length, texturesToCombine[0].format, true, false);
        for (int i = 0; i < texturesToCombine.Length; i++)
        {
            for (int m = 0; m < texturesToCombine[i].mipmapCount; m++)
            {
                Graphics.CopyTexture(texturesToCombine[i], 0, m, texture2DArray, i, m);
            }
        }
        
        AssetDatabase.CreateAsset(texture2DArray, path);
    }

    private Color[] FillColorArray(int width, int height, Color color)
    {
        Color[] outputColors = new Color[width * height];
        for (int i = 0; i < outputColors.Length; i++)
        {
            outputColors[i] = color;
        }

        return outputColors;
    }

    private Texture2D MakeTexture(int width, int height, TextureFormat format, Color color)
    {
        Texture2D outputTexture2D = new Texture2D(width, height, DefaultFormat.LDR, TextureCreationFlags.None);
        outputTexture2D.SetPixels(FillColorArray(texturesToCombine[0].width, texturesToCombine[0].height, color));
        outputTexture2D.Apply();
        EditorUtility.CompressTexture(outputTexture2D, format, TextureCompressionQuality.Normal);
        return outputTexture2D;
    }
}