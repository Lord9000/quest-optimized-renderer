Shader "Mobile VR/Lightmapped (ArrayTex)"
{
    Properties
    {
        [NoScaleOffset] _MainTexArray ("Texture Array", 2DArray) = "" {}
        _Color ("Tint", Color) = (1, 1, 1, 1)
        [HDR] _Emission ("Emission Color", Color) = (0, 0, 0, 0)
        _BumpScale ("Normal Scale", Float) = 1.0
        _Metallic ("Metallic", Range(0.0, 1.0)) = 0.0
        _Smoothness ("Smoothness", Range(0.0, 1.0)) = 0.0
        _Occlusion ("Occlusion", Range(0.0, 1.0)) = 1.0
        _Cutoff ("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

        [HideInInspector] _SrcBlend ("_SrcBlend", Float) = 1
		[HideInInspector] _DstBlend ("_DstBlend", Float) = 0
		[HideInInspector] _ZWrite ("_ZWrite", Float) = 1
        [HideInInspector] _AlphaToMask ("_AlphaToMask", Float) = 0
    }
    SubShader
    {
        // Lightmapper only pass
        Pass 
        {
			Tags 
            {
				"LightMode" = "Meta"
			}

			Cull Off
            Blend [_SrcBlend] [_DstBlend]
            ZWrite [_ZWrite]
            AlphaToMask [_AlphaToMask]

			CGPROGRAM

			#pragma vertex LightmappingVertexProgram
			#pragma fragment LightmappingFragmentProgram

			#include "Lightmapping.cginc"

			ENDCG
		}

        // Main forward pass
        Pass
        {
            Tags { "LightMode" = "ForwardBase" }
			Blend [_SrcBlend] [_DstBlend]
			ZWrite [_ZWrite]
            AlphaToMask [_AlphaToMask]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma require 2darray

            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog

            // ====================================================================================
            // Every shader tier disables some of the calculations
            // If overridden, the dynamic LOD system will not change the tier based on distance
            // TIER0 - everything
            // TIER1 - per vertex view pos, disabled box proj (if not overridden), disabled normals
            // TIER2 - disabled specular part
			#pragma multi_compile SHADER_TIER0 SHADER_TIER1 SHADER_TIER2
            
            #pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_ALPHA_TO_COVERAGE _RENDERING_FADE _RENDERING_TRANSPARENT
            #pragma shader_feature _MORE_PARAMETERS
            #pragma shader_feature _OVERRIDE_SHADER_TIER
            #pragma shader_feature _USE_PER_PIXEL_VIEW_DIR
            #pragma shader_feature _USE_BOX_PROJECTION
            #pragma shader_feature _USE_ALBEDO_COLOR
			#pragma shader_feature _USE_EMISSION_COLOR
            #pragma shader_feature _USE_BUMP_SCALE
            #pragma shader_feature _USE_OCCLUSION_MULTIPLIER
			#pragma shader_feature _USE_SMOOTHNESS_MULTIPLIER
            #pragma shader_feature _USE_METALLIC_MULTIPLIER
			#pragma shader_feature _METALLIC_MAP
            #pragma shader_feature _OCCLUSION_MAP
            #pragma shader_feature _EMISSION_MAP
            #pragma shader_feature _NORMAL_MAP

			#define DIFFUSE_IBL_MIP_LEVEL 5
            #define SPECULAR_IBL_MIP_COUNT 5
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
                #if defined(_NORMAL_MAP)
                    float4 tangent : TANGENT;
                #endif
                #if defined(LIGHTMAP_ON)
                    float2 uv1 : TEXCOORD1;
                #endif
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                half3 normal : TEXCOORD1;
                #if defined(_NORMAL_MAP)
                    half3 tangent : TEXCOORD2;
                    half3 binormal : TEXCOORD3;
                #endif
                #if defined(LIGHTMAP_ON)
                    float2 lightmapUV : TEXCOORD4;
                #endif
                #if defined(SHADER_TIER0) || defined(SHADER_TIER1)
                    half3 worldPos : NORMAL1;
                #endif
                #if defined(SHADER_TIER1)
                    #ifndef _USE_PER_PIXEL_VIEW_DIR
                        half3 viewDir : NORMAL2;
                    #endif
                #endif
                UNITY_FOG_COORDS(1)
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            half3 CreateBinormal (half3 normal, half3 tangent, half binormalSign) 
            {
                return cross(normal, tangent) * (binormalSign * unity_WorldTransformParams.w);
            }

            v2f vert (appdata v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv = v.uv;
                #if defined(SHADER_TIER0) || defined(SHADER_TIER1)
                    o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                #endif
                #if defined(SHADER_TIER1)
                    #ifndef _USE_PER_PIXEL_VIEW_DIR
                        o.viewDir = normalize(_WorldSpaceCameraPos - o.worldPos);
                    #endif
                #endif
                #if defined(_NORMAL_MAP)
                    o.tangent = UnityObjectToWorldDir(v.tangent.xyz);
                    o.binormal = CreateBinormal(o.normal, o.tangent, v.tangent.w);
                #endif
                #if defined(LIGHTMAP_ON)
                    o.lightmapUV = v.uv1 * unity_LightmapST.xy + unity_LightmapST.zw;
                #endif
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            UNITY_DECLARE_TEX2DARRAY(_MainTexArray);

            #if defined(_USE_ALBEDO_COLOR)
				half4 _Color;
			#endif

            #if defined(_USE_EMISSION_COLOR)
				half4 _Emission;
            #endif

            #if defined(_USE_BUMP_SCALE)
				half _BumpScale;
            #endif

            #if defined(_USE_OCCLUSION_MULTIPLIER)
				half _Occlusion;
            #endif

			#if defined(_USE_SMOOTHNESS_MULTIPLIER)
				half _Smoothness;
			#endif

            #if defined(_USE_METALLIC_MULTIPLIER)
				half _Metallic;
			#endif

			#if defined(_RENDERING_CUTOUT)
                half _Cutoff;
			#endif

            half3 BoxProjection (half3 direction, half3 position, half3 cubemapPosition, half3 boxMin, half3 boxMax) 
            {
                float3 factors = ((direction > 0 ? boxMax : boxMin) - position) / direction;
                float scalar = min(min(factors.x, factors.y), factors.z);
                return direction * scalar + (position - cubemapPosition);
            }

            half pow3(half val)
            {
                return val * val * val;
            }

            half pow4(half val)
            {
                return val * val * val * val;
            }

            half pow5(half val)
            {
                return val * val * val * val * val;
            }

            half3 fresnelSchlickRoughness(half cosTheta, half3 F0, half roughness)
            {
                //return F0 + (max(1.0 - roughness, F0) - F0) * (F0 / cosTheta); // pow(max(1.0 - cosTheta, 0.0), 5.0)
                return F0 + (max(1.0 - roughness, F0) - F0) * pow5(max(1.0 - cosTheta, 0.0));
                //return F0 + (max(1.0 - roughness, F0) - F0) * (0.04 / cosTheta);
                //return F0 + (max(1.0 - roughness, F0) - F0) * clamp(((0.05 / (cosTheta * 0.24) + 0.04) - 0.21), 0.0, 1.0);
            }
            
            half4 frag(v2f i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
                // Fragment output color
                half3 surfColor = 0.0, irradiance = 0.0, finalColor = 0.0;
                half occlusion = 1.0, metallic = 0.0, roughness = 1.0;

            	// ==== Albedo map sample ====
                half4 albedoMap = UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 0.0));

				#if defined(_RENDERING_CUTOUT)
					clip(albedoMap.a - _Cutoff);
				#endif

                #if defined(_USE_ALBEDO_COLOR)
                    surfColor = albedoMap * _Color;
                #else
                    surfColor = albedoMap;
                #endif

                // ==== Occlusion map sampling ====
				#if defined(_OCCLUSION_MAP)
                    occlusion = UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 3.0)).r;
				#endif

                // ==== Normal map sampling and calculation ====
				#if defined(_NORMAL_MAP) && defined(SHADER_TIER0)
                    half3 normalMap = 0.0;
                    // Sample normalmap and convert to linear space (Arraytexture limitation)
                    #if defined(_USE_BUMP_SCALE)
                        normalMap = UnpackNormalWithScale(half4(LinearToGammaSpace(UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 2.0))), 0.0), _BumpScale);
                    #else
                        normalMap = LinearToGammaSpace(UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 2.0))) * 2.0 - 1.0;
                    #endif

                    i.normal = normalize(
	                    normalMap.x * i.tangent +
	                    normalMap.y * i.binormal +
	                    normalMap.z * i.normal);
				#endif

            	// ==== Static ambient lightning sampling ====
                // TODO:
                // - ambient color
                // - light probes
				#if defined(LIGHTMAP_ON)
            		// If the object is affected by lightmap
					irradiance = DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.lightmapUV));
					#if defined(DIRLIGHTMAP_COMBINED)
						half4 lightDir = UNITY_SAMPLE_TEX2D_SAMPLER(unity_LightmapInd, unity_Lightmap, i.lightmapUV); // <--- Gets dominant light direction from lightmap
                        irradiance = DecodeDirectionalLightmap(irradiance, lightDir, i.normal);
					#endif
				#else
					// If the object is not affected by lightmap
            		irradiance = DecodeHDR(UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, i.normal, DIFFUSE_IBL_MIP_LEVEL), unity_SpecCube0_HDR); // <--- If object is not affected by lightmap, use IBL cubemap instead
				#endif

                // ==== Metallic map sampling ====
				#if defined(_METALLIC_MAP)
                    half4 metallicMap = UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 1.0));
            		// Extract metallic value from r channel and smoothness from a channel
                    #if defined(_USE_METALLIC_MULTIPLIER)
                        metallic = metallicMap.r *_Metallic;
                    #else
                        metallic = metallicMap.r;
                    #endif

                    #if defined(_USE_SMOOTHNESS_MULTIPLIER)
                        roughness = 1.0 - metallicMap.a * _Smoothness;
                    #else
                        roughness = 1.0 - metallicMap.a;
                    #endif
				#else
                    #if defined(_USE_METALLIC_MULTIPLIER)
                        metallic = _Metallic;
                    #endif
                    #if defined(_USE_SMOOTHNESS_MULTIPLIER)
                        roughness = 1.0 - _Smoothness;
                    #endif
                #endif

            	// ==== Indirect lightning PBR calculations ====
                #if (defined(_METALLIC_MAP) || defined(_USE_METALLIC_MULTIPLIER) || defined(_USE_SMOOTHNESS_MULTIPLIER)) && (defined(SHADER_TIER0) || defined(SHADER_TIER1))
                    #if defined(SHADER_TIER1)
                        #ifndef _USE_PER_PIXEL_VIEW_DIR
                            half3 viewDir = i.viewDir;
                        #else
                            half3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
                        #endif
                    #else
                        half3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
                    #endif
                    half3 reflectionDir = reflect(-viewDir, i.normal);
					#ifndef SHADER_TIER1
                        #if defined(_USE_BOX_PROJECTION)
                            reflectionDir = BoxProjection(reflectionDir, i.worldPos, unity_SpecCube0_ProbePosition, unity_SpecCube0_BoxMin, unity_SpecCube0_BoxMax);
                        #endif
					#endif
                    half4 probeSample = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectionDir, roughness * SPECULAR_IBL_MIP_COUNT);

                    half3 F0 = 0.04;
                    F0 = lerp(F0, surfColor, metallic);

                    half cosTheta = max(dot(i.normal, viewDir), 0.0);

                    half3 F = fresnelSchlickRoughness(cosTheta, F0, roughness);
                    half kD = 1.0 - F;
                    kD *= 1.0 - metallic;

                    // Diffuse component
                    half3 diffuse = irradiance * surfColor;

                    // Specular component
                    half3 prefilteredColor = DecodeHDR(probeSample, unity_SpecCube0_HDR).rgb;

                    half3 envBRDF = pow3(1.0 - max(roughness, cosTheta)) + F;
                    half3 specular = prefilteredColor * envBRDF;

                    finalColor = kD * diffuse + specular;
                    #if defined(_OCCLUSION_MAP)
                        #if defined(_USE_OCCLUSION_MULTIPLIER)
                            finalColor *= lerp(1.0, occlusion, _Occlusion);
                        #else
                            finalColor *= occlusion;
                        #endif
                    #else
                        finalColor *= occlusion;
                    #endif
                #else
                    finalColor = surfColor * irradiance;
                    #if defined(_OCCLUSION_MAP)
                        #if defined(_USE_OCCLUSION_MULTIPLIER)
                            finalColor *= lerp(1.0, occlusion, _Occlusion);
                        #else
						finalColor *= occlusion;
                        #endif
                    #endif
                #endif

                // ==== Emission map sampling ====
				#if defined(_EMISSION_MAP)
                    half3 emission = UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 4.0)).rgb;
                    #if defined(_USE_EMISSION_COLOR)
                        finalColor = max(finalColor, emission * _Emission);
                    #else
                        finalColor = max(finalColor, emission);
                    #endif
				#endif

            	// Fog
                UNITY_APPLY_FOG(i.fogCoord, finalColor);

                return half4(finalColor, albedoMap.a);
            }
            ENDCG
        }
    }

    CustomEditor "LightmappedMobileShaderGUI"
}