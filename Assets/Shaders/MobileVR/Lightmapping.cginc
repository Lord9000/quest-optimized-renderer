#if !defined(LIGHTMAPPING_INCLUDED)
    #define LIGHTMAPPING_INCLUDED
    
    #include "UnityPBSLighting.cginc"
    #include "UnityMetaPass.cginc"
    
    #pragma require 2darray
    
    #pragma shader_feature _EMISSION_MAP
    
    UNITY_DECLARE_TEX2DARRAY(_MainTexArray);
	half4 _Emission;
    
    struct VertexData
    {
        float4 vertex: POSITION;
        float2 uv: TEXCOORD0;
        float2 uv1: TEXCOORD1;
    };
    
    struct Interpolators
    {
        float4 pos: SV_POSITION;
        float2 uv: TEXCOORD0;
    };

	float GetDetailMask (Interpolators i) 
	{
		return 1.0;
	}
    
    float3 GetAlbedo(Interpolators i)
    {
        return UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 0.0));
    }
    
    float GetMetallic(Interpolators i)
    {
        return UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 1.0)).r;
    }
    
    float GetSmoothness(Interpolators i)
    {
        return UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 1.0)).a;
    }
    
    float3 GetEmission(Interpolators i)
    {
        #if defined(_EMISSION_MAP)
            return UNITY_SAMPLE_TEX2DARRAY(_MainTexArray, float3(i.uv.x, i.uv.y, 4.0)) * _Emission;
        #else
            return _Emission;
        #endif
    }
    
    Interpolators LightmappingVertexProgram(VertexData v)
    {
        Interpolators i;
        v.vertex.xy = v.uv1 * unity_LightmapST.xy + unity_LightmapST.zw;
        v.vertex.z = v.vertex.z > 0 ? 0.0001: 0;
        i.pos = UnityObjectToClipPos(v.vertex);
        i.uv = v.uv;
        return i;
    }
    
    float4 LightmappingFragmentProgram(Interpolators i): SV_TARGET
    {
        UnityMetaInput surfaceData;
        surfaceData.Emission = GetEmission(i);
        float oneMinusReflectivity;
        surfaceData.Albedo = DiffuseAndSpecularFromMetallic(GetAlbedo(i), GetMetallic(i), surfaceData.SpecularColor, oneMinusReflectivity);
        float roughness = SmoothnessToRoughness(GetSmoothness(i)) * 0.5;
	    surfaceData.Albedo += surfaceData.SpecularColor * roughness;
        return UnityMetaFragment(surfaceData);
    }
    
#endif