Shader "Custom/TestNormal"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _Normal("Normal Map", 2D) = "bump" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _Normal;

        struct Input
        {
            half3 tangent_input;
            half3 binormal_input;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert(inout appdata_full i, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input, o);

            half4 p_normal = mul(float4(i.normal, 0.0f), unity_WorldToObject);
            half3 p_tangent = UnityObjectToWorldDir(i.tangent);//mul(unity_ObjectToWorld, i.tangent);

            half3 normal_input = normalize(p_normal.xyz);
            half3 tangent_input = p_tangent;// normalize(p_tangent.xyz);
            half3 binormal_input = cross(p_normal.xyz, tangent_input.xyz) * i.tangent.w;

            o.tangent_input = tangent_input;
            o.binormal_input = binormal_input;
        }
        
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            //fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            //o.Albedo = c.rgb;
            //o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_MainTex));
            o.Emission = IN.tangent_input;
            // Metallic and smoothness come from slider variables
            o.Metallic = 0.0;
            o.Smoothness = 0.0;
            //o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
